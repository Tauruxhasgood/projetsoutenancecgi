package fr.formation.correction.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.correction.model.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Integer> {
	
	@Query("SELECT f FROM Filiere f WHERE f.libelle = :l")
	public Optional<Filiere> findByLibelle(@Param("l") String libelle);

}
