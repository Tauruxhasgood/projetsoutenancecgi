package fr.formation.correction.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Formation;

public interface FormationRepository extends JpaRepository<Formation, Integer>{
	
	public Optional<Formation> findByLibelle(String nom);

}
