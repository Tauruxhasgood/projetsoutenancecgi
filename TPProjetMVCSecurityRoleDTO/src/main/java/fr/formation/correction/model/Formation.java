package fr.formation.correction.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "formation_gen", sequenceName = "formation_seq", initialValue = 1, allocationSize = 1)
public class Formation {
	
	@Id
	@GeneratedValue(generator= "formation_gen")
	private Integer id;
	
	private String libelle;
	
	@OneToMany(mappedBy = "formation")
	@JsonIgnoreProperties("formation")
	private List<Personne> personne;

	public Formation() {
		super();
	}

	public Formation(Integer id, String libelle, List<Personne> personne) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.personne = personne;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Personne> getPersonne() {
		return personne;
	}

	public void setPersonne(List<Personne> personne) {
		this.personne = personne;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", libelle=" + libelle + ", personne=" + personne + "]";
	}
	
	

	
	
	
	
	
	

}
