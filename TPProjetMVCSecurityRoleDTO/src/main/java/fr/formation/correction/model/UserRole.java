package fr.formation.correction.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "user_role_gen", sequenceName = "user_role_seq", initialValue = 100, allocationSize = 1)
public class UserRole {
	@Id
	@GeneratedValue(generator = "user_role_gen")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "user_username")
	@JsonIgnoreProperties("roles")
	private User user;

	@Enumerated(EnumType.STRING)
	private Role role;

	public UserRole() {
		super();
	}
	
	public UserRole(User user, Role role) {
		this.user = user;
		this.role = role;
	}

	public UserRole(Integer id, User user, Role role) {
		super();
		this.id = id;
		this.user = user;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String toString() {
		return this.role.name();
	}
}
