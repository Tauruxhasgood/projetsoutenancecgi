package fr.formation.correction.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class User {

	@Id
	private String username;

	private String password;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	@JsonIgnoreProperties("user")
	private List<UserRole> roles;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="personne_id",referencedColumnName="id")
	private Personne personne;

	public User() {
		super();
	}
	
	public User(String username,String password) {
		this.username = username;
		this.password = password;
	}
	
	public User(String username,String password,Personne personne) {
		this.username = username;
		this.password = password;
		this.personne = personne;
	}

	

public User(String username, String password, List<UserRole> roles, Personne personne) {
		super();
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.personne = personne;
	}

	public String getUsername() {
	return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<UserRole> getRoles() {
		return roles;
	}
	
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}
	
	public Personne getPersonne() {
		return personne;
	}
	
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	
	
//	@JsonIgnore
//	public Collection<? extends GrantedAuthority> getRolesAsAuthorities() {
//		return this.roles.stream().map(r -> new SimpleGrantedAuthority(r.getRole().name()))
//				.collect(Collectors.toList());
//	}

	public UserDetails toUserDetails() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		for(UserRole role : roles) {
			GrantedAuthority ga = new SimpleGrantedAuthority(role.getRole().name());
			authorities.add(ga);
		}		
		
		return new org.springframework.security.core.userdetails.User(this.username, this.password,authorities);
	}

}
