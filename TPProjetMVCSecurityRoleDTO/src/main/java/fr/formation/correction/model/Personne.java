package fr.formation.correction.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "personne_gen", sequenceName = "personne_seq", initialValue = 300, allocationSize = 1)
public class Personne {

	@Id
	@GeneratedValue(generator = "personne_gen")
	private Integer id;

	private String nom;

	private String prenom;
	
	private String email;
	
	private String telephone;
	
	private LocalDate dateDeNaissance;
	
	private String adresse;
	
	@ManyToOne
	@JoinColumn(name= "formation_id")
	@JsonIgnoreProperties("personne")
	private Formation formation;

	@Enumerated(EnumType.STRING)
	private TypePersonne type;

	@ManyToOne
	@JoinColumn(name = "filiere_id")
	@JsonIgnoreProperties("stagiaires")
	private Filiere filiere;

	@OneToMany(mappedBy = "formateur")
	@JsonIgnoreProperties("formateur")
	private List<Module> modules;
	
    @OneToOne(mappedBy = "personne")
    @JsonIgnoreProperties("personne")
    private User user;

	public Personne() {
		super();
	}

	public Personne(Integer id, String nom, String prenom, String email, String telephone, LocalDate dateDeNaissance,
			String adresse, Formation formation, TypePersonne type, Filiere filiere, List<Module> modules, User user) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.dateDeNaissance = dateDeNaissance;
		this.adresse = adresse;
		this.formation = formation;
		this.type = type;
		this.filiere = filiere;
		this.modules = modules;
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public LocalDate getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(LocalDate dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public TypePersonne getType() {
		return type;
	}

	public void setType(TypePersonne type) {
		this.type = type;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", telephone="
				+ telephone + ", dateDeNaissance=" + dateDeNaissance + ", adresse=" + adresse + ", formation="
				+ formation + ", type=" + type + ", filiere=" + filiere+ "]";
	}
	
	



	
	
	

	
}
