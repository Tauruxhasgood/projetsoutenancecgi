package fr.formation.correction.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.User;
import fr.formation.correction.repository.UserRepository;

@Service
public class AuthService implements UserDetailsService {
	
	@Autowired
	private UserRepository ur;


	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> optU = ur.findByUsername(username);

		if(optU.isPresent()) {			
			return optU.get().toUserDetails();
		} else {
			throw new UsernameNotFoundException("Le nom d'utilisateur "+username+" n'existe pas");
		}
	}
	
	public UserDetails checkUser(User user) throws UsernameNotFoundException {
		Optional<User> optU = ur.findByUsername(user.getUsername());

		System.out.println(user.getPassword());
		System.out.println(optU.get().getPassword());
		if(optU.isPresent() & optU.get().getPassword().equals(user.getPassword())) {			
			return optU.get().toUserDetails();
		} else {
			throw new UsernameNotFoundException("Nom d'utilisateur ou le mot de passe incorrect");
		}
	}
	
	
	
}
