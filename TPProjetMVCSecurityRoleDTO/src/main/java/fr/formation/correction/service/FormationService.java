package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.Formation;
import fr.formation.correction.model.Personne;
import fr.formation.correction.repository.FormationRepository;
import fr.formation.correction.repository.PersonneRepository;

@Service
public class FormationService {

	@Autowired
	private FormationRepository formr;
	
	@Autowired
	private PersonneService ps;
	
	public List<Formation> findall(){
		return this.formr.findAll();
	}
	
	public Optional<Formation> getById(Integer id){
		return this.formr.findById(id);
	}
	
	public Optional<Formation> getByNom(String nom){
		return this.formr.findByLibelle(nom);
	}
	
	
	public void create(Formation f) {
		this.formr.save(f);
	}
	
	public void update(Formation f) {
		this.formr.save(f);
	}
	
	public Optional<Boolean> delete(Formation f) {
		try {
			this.formr.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(Integer id) {
		try {
			this.formr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public List<Personne> findStagiaireAskingFormation(Integer id){
		return ps.findStagiaireAskingFormation(id);
	}
}
