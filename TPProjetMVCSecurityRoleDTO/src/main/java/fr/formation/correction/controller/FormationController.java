package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Formation;
import fr.formation.correction.model.Personne;
import fr.formation.correction.service.FormationService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/formation")
public class FormationController {
	
	@Autowired
	FormationService fs;
	
	@GetMapping("/{id}")
	public Formation getById(@PathVariable Integer id) {
		
		return fs.getById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
		
	}
	
	@GetMapping("/byNom/{nom}")
	public Formation getByNom(@PathVariable String nom) {
		
		return fs.getByNom(nom)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
		
	}
	
	@GetMapping("")
	public List<Formation> findAll(){
		return fs.findall();
	}
	
	
	@PostMapping("")
	public void createFormation(@RequestBody Formation f) {
		fs.create(f);
	}
	
	@PutMapping
	public void updateFormation(@RequestBody Formation f) {
		fs.update(f);
	}
	
	@DeleteMapping("/{id}")
	public void deleteFormation(@PathVariable Integer id) {
		fs.delete(id)
			.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}
	
	@GetMapping("/stagiaire/{id}")
	public List<Personne> findStagiaireAskingFormation(@PathVariable Integer id){
		return fs.findStagiaireAskingFormation(id);
	}
	
	
	

}
