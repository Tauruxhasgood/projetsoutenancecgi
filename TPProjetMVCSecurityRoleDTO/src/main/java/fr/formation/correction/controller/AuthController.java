package fr.formation.correction.controller;




import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.correction.model.Personne;
import fr.formation.correction.model.User;
import fr.formation.correction.repository.UserRepository;
import fr.formation.correction.service.AuthService;
import fr.formation.correction.service.PersonneService;
import fr.formation.correction.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	AuthService as;
	
	@Autowired
	PersonneService ps;
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	AuthenticationManager authenticationManager;

	@PostMapping("/signin")
	public Personne authUser(@Valid @RequestBody User user){
		
		
		//return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
		
		//Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
	
		 	
			
			UsernamePasswordAuthenticationToken token =new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
			
			Authentication authentication = this.authenticationManager.authenticate(token);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
			
			UserDetails userDetails = this.as.loadUserByUsername(user.getUsername());
 
			List<String> roles = new ArrayList();
 
			for (GrantedAuthority authority : userDetails.getAuthorities()) {
				roles.add(authority.toString());
			}
			

			return ur.findByUsername(user.getUsername()).get().getPersonne();
			
	}
	

	
	
}
