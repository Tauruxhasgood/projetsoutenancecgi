package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.TypePersonne;
import fr.formation.correction.model.User;
import fr.formation.correction.service.AuthService;
import fr.formation.correction.service.UserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService service;
	
	@GetMapping
	public List<User> findAll(){
		return service.findAll();	}

	@GetMapping("/{name}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User getByUsername(@PathVariable String name) {
		return service.getByUsername(name).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le nom d'utilisateur n'existe pas"));
	}

	@PostMapping("")
	public User createUser(@RequestBody User u) {
		//u.getPersonne().setType(TypePersonne.STAGIAIRE);
		return service.create(u);
	}
	
	
}
