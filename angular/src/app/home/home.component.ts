import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Personne } from '../interfaces/personne.interface';
import { User } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  logFailed: string ='';

  user: User ={
    username: '',
    password: '',
    roles: []
  }

  invalidLogin = false

  constructor(private router: Router,
              private auth : UserService) { }

  ngOnInit(): void {

  }

  doLogin() {

    this.auth.login(this.user).subscribe(
      (res: Personne)=>{
      
        sessionStorage.setItem('personneId', (res.id == undefined ? 'No Id' : res.id));
        sessionStorage.setItem('Role', res.type);
        
        this.logFailed='';

        this.router.navigate(['/modules']).then(()=>window.location.reload());
      
      },
      (error) =>{
        this.user.username='';
        this.user.password='';
        this.logFailed='Identifiant ou mot de passe incorrect.';
      }
        
  );

  }

  doInscription() {
    this.router.navigate(['/formulaire'])
  }

}
