import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  role: string | null = sessionStorage.getItem('Role');

  constructor(private router: Router) { }

  ngOnInit(): void {
    if(this.role == null){
      sessionStorage.setItem('Role', 'null');
      location.reload();
    }
  }

  logout(){
    sessionStorage.setItem('personneId', 'null');
    sessionStorage.setItem('Role', 'null');
  }

}
