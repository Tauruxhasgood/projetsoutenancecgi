import { Component, OnInit } from '@angular/core';
import { Formation } from 'src/app/interfaces/formation';
import { FormationService } from 'src/app/services/formation.service';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css']
})
export class FormationComponent implements OnInit {
  formations: Formation[] = [];

  

  formation: Formation = {
    libelle: ''
  }

  i = this.formation.id;

  constructor(private fs: FormationService) { }

  ngOnInit(): void {
    this.fs.getAll()
    .subscribe((formations: Formation[]) => {
      this.formations = formations
      console.log('info this.formation', this.formations);
      
    });
  }

  handleSubmit() {
    console.log('Addinf formation ...');
    // if(this.formations == this.formation.libelle){}
    this.fs.post(this.formation).pipe()
    .subscribe(() => {
      location.reload()
    }) 
}

  onDelete(id?: number) {
      console.log('delete ok...');
      if(id) {
        this.fs.delete(id).subscribe(() => {
          location.reload()
        })
      }
    }

}
