import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sous-menu',
  templateUrl: './sous-menu.component.html',
  styleUrls: ['./sous-menu.component.css']
})
export class SousMenuComponent implements OnInit {

  role: string | null = sessionStorage.getItem('Role');

  constructor() { }

  ngOnInit(): void {
  }

  logout(){
    sessionStorage.setItem('personneId', 'null');
    sessionStorage.setItem('Role', 'null');
  }
}
