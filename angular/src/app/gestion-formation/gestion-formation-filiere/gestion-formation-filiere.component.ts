import { Component, OnInit } from '@angular/core';
import { FilierePost } from 'src/app/interfaces/filiere';
import { Formation } from 'src/app/interfaces/formation';
import { Personne } from 'src/app/interfaces/personne.interface';
import { Stagiaire } from 'src/app/interfaces/stagiaire';
import { FiliereService } from 'src/app/services/filiere.service';
import { FormationService } from 'src/app/services/formation.service';
import { PersonneService } from 'src/app/services/personne.service';
import { FormsModule } from '@angular/forms';
import { waitForAsync } from '@angular/core/testing';

@Component({
  selector: 'app-gestion-formation-filiere',
  templateUrl: './gestion-formation-filiere.component.html',
  styleUrls: ['./gestion-formation-filiere.component.css']
})
export class GestionFormationFiliereComponent implements OnInit {

  formations: Formation[] = [];

  filiere: FilierePost = {
    libelle: '',
    stagiaires: [],
    formation: {id: 1},
    modules: []
  }

  isChecked: boolean =false;

  personnes: Personne[] = [];

  stagiaires: Personne[] = [];

  stagiairesToPost: Personne[]= [];

  idFiliere: any=0;

  // {id: 1, nom: "dupont", prenom: "paul", dateInscription: "2022-01-01"},
  //   {id: 1, nom: "dutreuil", prenom: "julien", dateInscription: "2022-01-10"},
  //   {id: 1, nom: "turlu", prenom: "marine", dateInscription: "2022-01-15"}


  constructor(
    private formas: FormationService,
    private fs: FiliereService,
    private ps: PersonneService
  ) { }

  ngOnInit(): void {
    this.formas.getAll()    
    .subscribe((formations: Formation[]) => {      
      this.formations = formations;
      if(this.filiere.formation && this.formations[0].id) {
        this.filiere.formation.id = this.formations[0].id;
      }
    });

    this.ps.getStagiairesAskingFormation(this.filiere.formation.id).subscribe((res: Personne[]) => {
      this.stagiaires=res;
    })


  }

  handleSubmit(){
    
    console.log('Adding filiere');
    this.fs.post(this.filiere)
    .subscribe(res=>
      {
        this.idFiliere=res;
      }
      );
    
   
    setTimeout(()=>{

      // console.log('Keep filiere id');
      // this.fs.getByLibelle(this.filiere.libelle).subscribe(res=> this.idFiliere=res.id==undefined? 0: res.id);

      console.log(this.idFiliere);
      console.log(this.stagiairesToPost);
    
      console.log('setfiliere to stagiaires');
      if(this.idFiliere!=0){
        this.fs.setStagiairesToFiliere(this.stagiairesToPost, this.idFiliere).subscribe(()=>
          location.reload()
        );
      }
    },1000);
  }

  handleChange(){
    this.ps.getStagiairesAskingFormation(this.filiere.formation.id).subscribe((res: Personne[]) => {
      this.stagiaires=res;
    })
  }

  checkChange(event:any, index: number){
    //let identifiant = parseInt(id==undefined ? '0':id)



    if(event.currentTarget.checked){
      this.stagiairesToPost.push(this.stagiaires[index]);
    }
    else{
      this.stagiairesToPost=this.stagiairesToPost.filter(item=> item!=this.stagiairesToPost[index]);
    }

  }

}
