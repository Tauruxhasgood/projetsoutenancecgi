import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionFormationFiliereComponent } from './gestion-formation-filiere.component';

describe('GestionFormationFiliereComponent', () => {
  let component: GestionFormationFiliereComponent;
  let fixture: ComponentFixture<GestionFormationFiliereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionFormationFiliereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionFormationFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
