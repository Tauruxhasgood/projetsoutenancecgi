import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionFormationStagiaireComponent } from './gestion-formation-stagiaire.component';

describe('GestionFormationStagiaireComponent', () => {
  let component: GestionFormationStagiaireComponent;
  let fixture: ComponentFixture<GestionFormationStagiaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionFormationStagiaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionFormationStagiaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
