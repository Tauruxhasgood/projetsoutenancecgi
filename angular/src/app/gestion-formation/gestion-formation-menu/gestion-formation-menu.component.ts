import { Component, OnInit } from '@angular/core';
import { Formation } from 'src/app/interfaces/formation';
import { Personne } from 'src/app/interfaces/personne.interface';
import { FormationService } from 'src/app/services/formation.service';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-gestion-formation-menu',
  templateUrl: './gestion-formation-menu.component.html',
  styleUrls: ['./gestion-formation-menu.component.css']
})
export class GestionFormationMenuComponent implements OnInit {
  formations: Formation[] = [];
  personne : Personne[]= [];
  
  submit = false;

  formation: Formation = {
    libelle: ''
  }

  constructor(private fs: FormationService,
    private pr : PersonneService) { }

  ngOnInit(): void {

    this.pr.getAllStagiaire()
    .subscribe(((personne: Personne[]) => {
      this.personne= personne;
    }))
    
  }
  handleSubmit(){
    
    

  
  }

  }


