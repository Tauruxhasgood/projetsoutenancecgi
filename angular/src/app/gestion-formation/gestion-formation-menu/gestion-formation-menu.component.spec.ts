import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionFormationMenuComponent } from './gestion-formation-menu.component';

describe('GestionFormationMenuComponent', () => {
  let component: GestionFormationMenuComponent;
  let fixture: ComponentFixture<GestionFormationMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionFormationMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionFormationMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
