import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';

import { GestionFormationMenuComponent } from './gestion-formation-menu/gestion-formation-menu.component';
import { FormationComponent } from './formation/formation.component';
import { GestionFormationFiliereComponent } from './gestion-formation-filiere/gestion-formation-filiere.component';
import { GestionFormationModuleComponent } from './gestion-formation-module/gestion-formation-module.component';
import { SousMenuComponent } from './sous-menu/sous-menu.component';
import { GestionFormationStagiaireComponent } from './gestion-formation-stagiaire/gestion-formation-stagiaire.component';

@NgModule({
  declarations: [
    GestionFormationMenuComponent,
    FormationComponent,
    GestionFormationFiliereComponent,
    GestionFormationModuleComponent,
    SousMenuComponent,
    GestionFormationStagiaireComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  exports: [
    GestionFormationMenuComponent,
    SousMenuComponent,
  ]
})
export class GestionFormationModule { }
