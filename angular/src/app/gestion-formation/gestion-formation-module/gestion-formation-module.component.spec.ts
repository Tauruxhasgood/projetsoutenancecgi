import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionFormationModuleComponent } from './gestion-formation-module.component';

describe('GestionFormationModuleComponent', () => {
  let component: GestionFormationModuleComponent;
  let fixture: ComponentFixture<GestionFormationModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionFormationModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionFormationModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
