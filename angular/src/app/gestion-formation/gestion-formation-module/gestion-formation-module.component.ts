import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Filiere } from 'src/app/interfaces/filiere';
import { Module, ModulePost } from 'src/app/interfaces/module';
import { FiliereService } from 'src/app/services/filiere.service';
import { ModuleService } from 'src/app/services/module.service';

interface Formateur { 
  id: number;
  nom: string;
  prenom: string;
};

@Component({
  selector: 'app-gestion-formation-module',
  templateUrl: './gestion-formation-module.component.html',
  styleUrls: ['./gestion-formation-module.component.css']
})
export class GestionFormationModuleComponent implements OnInit {
  filieres: Filiere [] = [];

  module: ModulePost = {
    libelle: "",
    dateDebut: "",
    dateFin: "",
    filiere: {id: 1},
    formateur: null
  }

  formateurs: Formateur[] = [
    {id: 1, nom: "Baggio", prenom: "Roberto"},
    {id: 2, nom: "Truc", prenom: "Machin"},
    {id: 3, nom: "Mohammoud", prenom: "Jean"},
  ];


  constructor(
    private fs: FiliereService,
    private ms: ModuleService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.fs.getAll()
      .subscribe((filieres: Filiere[]) => {
        this.filieres = filieres;
        if (this.module.filiere && this.filieres[0].id) {
          this.module.filiere.id = this.filieres[0].id;
        }

        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          this.ms.getById(Number(id))
            .subscribe((module: Module) => {
              this.module.id = module.id;
              this.module.libelle = module.libelle;
              this.module.dateDebut = module.dateDebut;
              this.module.dateFin = module.dateFin;
    
              if (module.filiere.id) {
                this.module.filiere.id = module.filiere.id;
              }
            })
        }
      })
  }

  handleSubmit() {
      console.log('Adding module...');
      this.ms.post(this.module)
        .subscribe(() => {
          location.reload()
        })
    
  }

}
