import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StagaireviewComponent } from './stagaireview/stagaireview.component';
import { FormateurviewComponent } from './formateurview/formateurview.component';



@NgModule({
  declarations: [
    StagaireviewComponent,
    FormateurviewComponent 

  ],
  imports: [
    CommonModule
  ],
  exports : [StagaireviewComponent,
    FormateurviewComponent ]
})
export class StagaireFormateurModule { }
