import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormateurviewComponent } from './formateurview.component';

describe('FormateurviewComponent', () => {
  let component: FormateurviewComponent;
  let fixture: ComponentFixture<FormateurviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormateurviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormateurviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
