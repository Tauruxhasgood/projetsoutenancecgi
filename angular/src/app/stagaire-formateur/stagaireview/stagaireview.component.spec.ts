import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StagaireviewComponent } from './stagaireview.component';

describe('StagaireviewComponent', () => {
  let component: StagaireviewComponent;
  let fixture: ComponentFixture<StagaireviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StagaireviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagaireviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
