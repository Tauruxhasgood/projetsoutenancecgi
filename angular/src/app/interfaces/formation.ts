import { Personne } from "./personne.interface";

export interface Formation {
    id?: number;
    libelle: string;
    personne?: Personne[];
}