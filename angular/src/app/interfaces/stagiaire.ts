export interface Stagiaire {
    id: number;
    nom: string;
    prenom: string;
    dateInscription: string;
}