
import { Filiere } from "./filiere";
import { Formation } from "./formation";
import { Module } from "./module";

export interface Personne {
    id?: string;
    nom: string;
    prenom: string;
    email: string;
    telephone: string;
    dateDeNaissance: string;
    adresse: string;
    formation?: Formation;
    type: string;
    filiere?: Filiere;
    modules?: Module[];
  }
  