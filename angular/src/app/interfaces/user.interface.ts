import { Personne } from "./personne.interface";
import { Role } from "./role.interface";

export interface User {
    username: string;
    password: string;
    roles?: Role[];
    personne?: Personne;
  }
  