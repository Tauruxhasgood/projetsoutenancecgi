import { Module } from "./module";

type EntityId = {
  id: number;
}

export interface FilierePost {
  id?: number;
  libelle: string;
  modules: [];
  stagiaires: [];
  formation: EntityId;
}

export interface Filiere {
  id?: number;
  libelle: string;
  modules?: Module[];
  stagiaires: [];
}