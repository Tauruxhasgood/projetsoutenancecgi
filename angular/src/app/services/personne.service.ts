import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Formation } from '../interfaces/formation';
import { Personne } from '../interfaces/personne.interface';

const API=API_URL+'/personne';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  constructor(private http: HttpClient) { }

  getStagiairesAskingFormation(id: number): Observable<Personne[]> {
    return this.http.get<Personne[]>(API +'/stagiaires/'+ id);
  }
  
  getAll(): Observable<Personne[]> {
    return this.http.get<Personne[]>(API);
    
  }

  getAllStagiaire(): Observable<Personne[]> {
    return this.http.get<Personne[]>(API + '/stagiaires');
  }
 
  getById(id: number): Observable<Personne> {
   return this.http.get<Personne>(API + '/' + id);

  }

  post(personne: Personne) {
    return this.http.post(API, personne);
  }

  update(personne: Personne) {
    return this.http.put(API, personne);
  }

  delete(id: number) {
    return this.http.delete(API + '/' + id);
  }

}
