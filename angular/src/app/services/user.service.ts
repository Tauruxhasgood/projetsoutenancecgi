import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Personne } from '../interfaces/personne.interface';
import { User } from '../interfaces/user.interface';

const API=API_URL+'/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(user: User){
    
    return this.http.post(API_URL+'/auth/signin', user).pipe(
      map(res=>{ return res as Personne})
    );
  }

  
}
