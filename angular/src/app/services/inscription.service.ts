import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/config/settings';
import { Personne } from '../interfaces/personne.interface';
import { User } from '../interfaces/user.interface';

const API_USER= API_URL +'/user';
const API_PERS= API_URL +'/personne';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private http: HttpClient) { }

  postUser(user: User){
    return this.http.post(API_USER, user)
  }

  postPers(personne: Personne){
    return this.http.post(API_PERS, personne);
  }

}
