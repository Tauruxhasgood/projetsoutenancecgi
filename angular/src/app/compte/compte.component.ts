import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Personne } from '../interfaces/personne.interface';
import { PersonneService } from '../services/personne.service';


@Component({
  selector: 'compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {

  personne: Personne | null = null;
  personneId: string | null = sessionStorage.getItem('personneId');
  role: string | null = sessionStorage.getItem('Role');

  constructor(private ps: PersonneService,
    private router: Router) { }

  ngOnInit(): void {

    if(this.personneId != null){
      this.ps.getById(parseInt(this.personneId)).subscribe(res=> this.personne=res);
    }

  }

  

}
