import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder , FormGroup,  FormControl,Validators,ValidatorFn, AbstractControl} from '@angular/forms';
import { InscriptionService } from '../services/inscription.service';
import { User } from '../interfaces/user.interface';
import { Personne } from '../interfaces/personne.interface';
import {Role} from '../interfaces/role.interface';
import { Observable } from 'rxjs/internal/Observable';
import { FormationService } from '../services/formation.service';
import { Formation } from '../interfaces/formation';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {

  //le nom a changer par formations
  formations: Formation[] = [];
  personneForm! : FormGroup;
  personnePreview$!: Observable<Personne>;
  user! : User;
  clickMessage = '';
  password = '';
  username ='';
  submit = false;
  urlRegexE! : RegExp;
  urlRegexT! : RegExp;

  formationSelected: Formation = {
    id: 0,
    libelle: '',
    personne: []
  }

  id: number=0;

  personneACreer: Personne ={
    nom: '',
    prenom: '',
    email: '',
    telephone: '',
    dateDeNaissance: '',
    adresse: '',
    type: '',
    formation: {} as Formation
  };
  
    
  constructor(private fs: FormationService,
    private router: Router,
    private formBuilder : FormBuilder,
    private is: InscriptionService) { }

  ngOnInit(): void {

    this.urlRegexT=  /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
    this.urlRegexE = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    //a changer avec le methode get all formation
    this.fs.getAll()
      .subscribe((formation: Formation[]) => {
        this.formations = formation;
      });


    this.personneForm =  this.formBuilder.group({
      nom: [null, [Validators.required]],
      prenom: [null,[Validators.required] ],
      email: [null, [Validators.required, Validators.pattern(this.urlRegexE)]],
      telephone: [null, [Validators.required, Validators.pattern(this.urlRegexT)]],
      dateDeNaissance: [null, [Validators.required]],
      adresse: [null, [Validators.required]],
      formation: [null, [Validators.required]],
    });

    }



    onSubmitForm() {

      const personne = this.personneForm.value;

      this.id = personne.formation;


      this.fs.getById(this.id).subscribe(
        (res: Formation)=> {
          console.log('Resultat Requete : ' + res);
          this.formationSelected = res;
        }
      );

      this.personneACreer = personne;

      this.personneACreer.formation=this.formationSelected;
      this.personneACreer.formation.personne = [];
      this.personneACreer.type = "STAGIAIRE";
      this.personneACreer.formation.id = this.id;

      const user :User = {
        username: this.personneForm.value.prenom[0] + this.personneForm.value.nom,
        password : 'aze123'+this.personneForm.value.nom[0],
        roles : [{role :"ROLE_USER"}],
        personne: this.personneACreer

      };



        this.is.postUser(user).subscribe(() => {
          const password = user.password;
          const username = user.username;});

    this.submit = true;
    this.clickMessage = "Hello : ";
    this.username = user.username;
    this.password = user.password;

  }

  
}

